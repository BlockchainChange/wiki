# BlockChange [BCCI] #
A JavaScript experimental blockchain API.

## [Issues](https://bitbucket.org/BlockchainChange/wiki/issues) ##


## Help ##


1) [Introduction](./Introductions.md) Introduction to BlockChange
1) [Whitepaper](./Whitepaper.md) Blockchange [BCCI] Whitepaper
1) [BCCI Node](./Nodes.md) Blockchange Node
1) [BCCI Node Cli](./NodeCli.md) Blockchange Node Cli
1) [BCCI Core Wallet](./CoreWallet.md) Blockchange Core Electron Wallet
1) [BCCI Web Wallet](./WebWallet.md) Blockchange React Wallet
1) [BCCI Core Explorer](./CoreExplorer.md) Blockchange Core Electron Chain Explorer
1) [BCCI Web Explorer](./WebExplorer.md) Blockchange Explorer
1) [BCCI Cli Miner](./CliMiner.md) Blockchange Cli Miner
1) [BCCI Miner Core](./MinerCore.md) Blockchange Miner Core Electron  

***


## Downloads ##


  ### _Windows_ ###
  1) [Node]()
  1) [Node Cli]()
  1) [Wallet Core]()
  1) [Miner Cli]()
  1) [Miner Core]()  

  ### _Linux_ ###
  1) [Node]()
  1) [Node Cli]()
  1) [Wallet Core]()
  1) [Miner Cli]()
  1) [Miner Core]()  

  ### _Apple_ ###

  1) [Node]()
  1) [Node Cli]()
  1) [Wallet Core]()
  1) [Miner Cli]()
  1) [Miner Core]()  

****  
  
###### If you find an error or would like to contribute please contact Blockchange support for instructions.  
  
#### Notes : ####
```$ git clone https://bitbucket.org/BlockchainChange/wiki.git/wiki ```  
  